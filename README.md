# telegram-bot-api-types.rs

Provides all the types described in the [API docs][], (de)serializable via
[serde][].

[api docs]: https://core.telegram.org/bots/api
[serde]: https://serde.rs/

This will reach 1.0 when all the types are described. Until then, expect lots of
missing things.
