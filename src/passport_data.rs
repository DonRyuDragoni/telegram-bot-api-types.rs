use super::{
    Array,
    EncryptedCredentials,
    EncryptedPassportElement,
};

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct PassportData {
    pub data:        Array<EncryptedPassportElement>,
    pub credentials: EncryptedCredentials,
}
