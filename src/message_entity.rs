use super::{
    Integer,
    User,
};

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct MessageEntity {
    #[cfg_attr(
        any(feature = "impl_serialize", feature = "impl_deserialize"),
        serde(rename = "type")
    )]
    pub type_: String,

    pub offset: Integer,
    pub length: Integer,
    pub url:    Option<String>,
    pub user:   Option<User>,
}
