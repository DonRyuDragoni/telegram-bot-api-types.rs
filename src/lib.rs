#[cfg(any(feature = "impl_serialize", feature = "deserialize"))]
#[macro_use]
extern crate serde_derive;

#[cfg(any(feature = "impl_serialize", feature = "deserialize"))]
extern crate serde;

pub type Integer = i32;
pub type Float = f32;
pub type Array<T> = Vec<T>;
pub type True = bool;

mod animation;
mod audio;
mod callback_query;
mod chat;
mod chat_photo;
mod chosen_inline_result;
mod contact;
mod document;
mod game;
mod inline_query;
mod location;
mod mask_position;
mod message;
mod message_entity;
mod photo_size;
mod sticker;
mod sticker_set;
mod update;
mod user;
mod user_profile_photos;
mod venue;
mod video;
mod video_note;
mod voice;

// payments
mod invoice;
mod labeled_price;
mod order_info;
mod pre_checkout_query;
mod shipping_address;
mod shipping_option;
mod shipping_query;
mod successful_payment;

// passport
mod encrypted_credentials;
mod encrypted_passport_element;
mod passport_data;
mod passport_file;
mod passport_element_error;
mod passport_element_error_data_field;
mod passport_element_error_front_side;
mod passport_element_error_reverse_side;
mod passport_element_error_selfie;
mod passport_element_error_file;
mod passport_element_error_files;
mod passport_element_error_translation_file;
mod passport_element_error_translation_files;
mod passport_element_error_unspecified;

pub use animation::*;
pub use audio::*;
pub use callback_query::*;
pub use chat::*;
pub use chat_photo::*;
pub use chosen_inline_result::*;
pub use contact::*;
pub use document::*;
pub use game::*;
pub use inline_query::*;
pub use invoice::*;
pub use labeled_price::*;
pub use location::*;
pub use mask_position::*;
pub use message::*;
pub use message_entity::*;
pub use photo_size::*;
pub use sticker::*;
pub use sticker_set::*;
pub use update::*;
pub use user::*;
pub use user_profile_photos::*;
pub use venue::*;
pub use video::*;
pub use video_note::*;
pub use voice::*;

// payments
pub use invoice::*;
pub use labeled_price::*;
pub use order_info::*;
pub use pre_checkout_query::*;
pub use shipping_address::*;
pub use shipping_option::*;
pub use shipping_query::*;
pub use successful_payment::*;

// passport
pub use encrypted_credentials::*;
pub use encrypted_passport_element::*;
pub use passport_data::*;
pub use passport_file::*;
pub use passport_element_error::*;
pub use passport_element_error_data_field::*;
pub use passport_element_error_front_side::*;
pub use passport_element_error_reverse_side::*;
pub use passport_element_error_selfie::*;
pub use passport_element_error_file::*;
pub use passport_element_error_files::*;
pub use passport_element_error_translation_file::*;
pub use passport_element_error_translation_files::*;
pub use passport_element_error_unspecified::*;
