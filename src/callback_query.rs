use super::{
    message::Message,
    user::User,
};

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct CallbackQuery {
    pub pubid:             String,
    pub pubfrom:           User,
    pub message:           Option<Message>,
    pub inline_message_id: Option<String>,
    pub chat_instance:     String,
    pub data:              Option<String>,
    pub game_short_name:   Option<String>,
}
