#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct PassportElementErrorFrontSide {
    pub source: String,

    #[cfg_attr(
        any(feature = "impl_serialize", feature = "impl_deserialize"),
        serde(rename = "type")
    )]
    pub type_: String,

    pub file_hash: String,
    pub message:   String,
}
