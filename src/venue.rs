use super::Location;

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct Venue {
    pub location:        Location,
    pub title:           String,
    pub address:         String,
    pub foursquare_id:   Option<String>,
    pub foursquare_type: Option<String>,
}
