use super::Float;

// TODO: should this implement PartialEq?
#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct MaskPosition {
    pub point:   String,
    pub x_shift: Float,
    pub y_shift: Float,
    pub scale:   Float,
}
