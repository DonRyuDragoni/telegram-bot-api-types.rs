#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct PassportElementErrorDataField {
    pub source: String,

    #[cfg_attr(
        any(feature = "impl_serialize", feature = "impl_deserialize"),
        serde(rename = "type")
    )]
    pub type_: String,

    pub field_name: String,
    pub data_hash:  String,
    pub message:    String,
}

#[cfg(test)]
mod tests {
    #[cfg(any(feature = "impl_serialize", feature = "impl_deserialize"))]
    extern crate serde_json;

    use super::*;

    #[cfg(any(feature = "impl_serialize", feature = "impl_deserialize"))]
    #[test]
    fn simulated_response() {
        let simulated_response = "{
               \"source\"     : \"my \
                                  dealer won't tell me\",
               \"type\"       : \"this is not a valid type\",
               \"field_name\" : \"major general\",
               \"data_hash\"  : \"harsh hash\",
               \"message\"    \
                                  : \"this is not a drill\"
             }";

        let deserialized: PassportElementErrorDataField =
            serde_json::from_str(simulated_response).unwrap();

        assert_eq!(deserialized.source, "my dealer won't tell me");
        assert_eq!(deserialized.type_, "this is not a valid type");
        assert_eq!(deserialized.field_name, "major general");
        assert_eq!(deserialized.data_hash, "harsh hash");
        assert_eq!(deserialized.message, "this is not a drill");
    }
}
