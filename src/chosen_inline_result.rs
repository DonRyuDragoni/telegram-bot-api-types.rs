#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct ChosenInlineResult {
    pub result_id:         String,
    pub from:              String,
    pub location:          Option<String>,
    pub inline_message_id: Option<String>,
    pub query:             Option<String>,
}
