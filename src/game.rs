use super::{
    Animation,
    Array,
    MessageEntity,
    PhotoSize,
};

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct Game {
    pub title:         String,
    pub description:   String,
    pub photo:         Array<PhotoSize>,
    pub text:          Option<String>,
    pub text_entities: Option<MessageEntity>,
    pub animation:     Option<Animation>,
}
