use super::Integer;

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct User {
    pub id:            Integer,
    pub is_bot:        bool,
    pub first_name:    String,
    pub last_name:     Option<String>,
    pub username:      Option<String>,
    pub language_code: Option<String>,
}
