use super::{
    ShippingAddress,
    User,
};

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct ShippingQuery {
    pub id:               String,
    pub from:             User,
    pub invoice_payload:  String,
    pub shipping_address: ShippingAddress,
}
