use super::{
    Array,
    PassportFile,
};

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct EncryptedPassportElement {
    #[cfg_attr(
        any(feature = "impl_serialize", feature = "impl_deserialize"),
        serde(rename = "type")
    )]
    pub type_: String,

    pub data:         Option<String>,
    pub phone_number: Option<String>,
    pub email:        Option<String>,
    pub files:        Option<Array<PassportFile>>,
    pub front_size:   Option<PassportFile>,
    pub reverse_side: Option<PassportFile>,
    pub selfie:       Option<PassportFile>,
    pub translation:  Option<Array<PassportFile>>,
    pub hash:         String,
}
