use super::{
    Integer,
    MaskPosition,
    PhotoSize,
};

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct Sticker {
    pub file_id:       String,
    pub width:         Integer,
    pub height:        Integer,
    pub thumb:         Option<PhotoSize>,
    pub emoji:         Option<String>,
    pub set_name:      Option<String>,
    pub mask_position: Option<MaskPosition>,
    pub file_size:     Option<Integer>,
}
