#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct ShippingAddress {
    pub country_code: String,
    pub state:        String,
    pub city:         String,
    pub street_line1: String,
    pub street_line2: String,
    pub post_code:    String,
}
