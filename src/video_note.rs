use super::{
    Integer,
    PhotoSize,
};

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct VideoNote {
    pub file_id:   Integer,
    pub length:    Integer,
    pub duration:  Integer,
    pub thumb:     Option<PhotoSize>,
    pub file_size: Option<Integer>,
}
