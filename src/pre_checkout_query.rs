use super::{
    Integer,
    OrderInfo,
    User,
};

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct PreCheckoutQuery {
    pub id:                 String,
    pub from:               User,
    pub currency:           String,
    pub total_amount:       Integer,
    pub invoice_payload:    String,
    pub shipping_option_id: Option<String>,
    pub order_info:         Option<OrderInfo>,
}
