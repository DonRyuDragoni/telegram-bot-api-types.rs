use super::Integer;

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct Voice {
    pub file_id:    String,
    pub diration:   Integer,
    pub mime_typep: Option<String>,
    pub file_size:  Option<Integer>,
}
