use super::{
    Integer,
    PhotoSize,
};

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct Video {
    pub file_id:   String,
    pub width:     Integer,
    pub height:    Integer,
    pub duration:  Integer,
    pub thumb:     Option<PhotoSize>,
    pub mime_type: Option<String>,
    pub file_size: Option<Integer>,
}
