use super::ShippingAddress;

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct OrderInfo {
    pub name:             Option<String>,
    pub phone_number:     Option<String>,
    pub email:            Option<String>,
    pub shipping_address: Option<ShippingAddress>,
}
