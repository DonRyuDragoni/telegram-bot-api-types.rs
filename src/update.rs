use super::{
    CallbackQuery,
    ChosenInlineResult,
    InlineQuery,
    Message,
    PreCheckoutQuery,
    ShippingQuery,
};

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct Update {
    pub update_id:            i32,
    pub message:              Option<Message>,
    pub edited_message:       Option<Message>,
    pub channel_post:         Option<Message>,
    pub edited_channel_post:  Option<Message>,
    pub inline_query:         Option<InlineQuery>,
    pub chosen_inline_result: Option<ChosenInlineResult>,
    pub callback_query:       Option<CallbackQuery>,
    pub shipping_query:       Option<ShippingQuery>,
    pub pre_checkout_query:   Option<PreCheckoutQuery>,
}
