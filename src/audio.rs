use super::{
    Integer,
    PhotoSize,
};

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct Audio {
    pub file_id:   String,
    pub duration:  Integer,
    pub performer: Option<String>,
    pub title:     Option<String>,
    pub mime_type: Option<String>,
    pub file_size: Option<Integer>,
    pub thumb:     Option<PhotoSize>,
}
