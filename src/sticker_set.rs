use super::{
    Array,
    Sticker,
};

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_totaleq", derive(Eq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct StickerSet {
    pub name:           String,
    pub title:          String,
    pub contains_masks: bool,
    pub stickers:       Array<Sticker>,
}
