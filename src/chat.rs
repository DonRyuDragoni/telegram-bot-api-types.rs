use super::{
    ChatPhoto,
    Integer,
    Message,
};

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct Chat {
    pub id: Integer,

    #[cfg_attr(
        any(feature = "impl_serialize", feature = "impl_deserialize"),
        serde(rename = "type")
    )]
    pub type_: String,

    pub title:                          Option<String>,
    pub username:                       Option<String>,
    pub first_name:                     Option<String>,
    pub last_name:                      Option<String>,
    pub all_members_are_administrators: Option<bool>,
    pub photo:                          Option<ChatPhoto>,
    pub description:                    Option<String>,
    pub invite_link:                    Option<String>,
    pub pinned_message:                 Option<Box<Message>>,
    pub sticker_set_name:               Option<String>,
    pub can_set_sticket_set:            Option<bool>,
}
