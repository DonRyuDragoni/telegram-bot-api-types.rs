use super::{
    Location,
    User,
};

#[cfg_attr(feature = "impl_serialize", derive(Serialize))]
#[cfg_attr(feature = "impl_deserialize", derive(Deserialize))]
#[cfg_attr(feature = "impl_partialeq", derive(PartialEq))]
#[cfg_attr(feature = "impl_debug", derive(Debug))]
pub struct InlineQuery {
    pub id:       String,
    pub from:     User,
    pub location: Option<Location>,
    pub query:    String,
    pub offset:   String,
}
